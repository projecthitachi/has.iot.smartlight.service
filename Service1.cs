﻿using Microsoft.AspNet.SignalR.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.smartlight.service
{
    public partial class Service1 : ServiceBase
    {
        #region variable
        private static MqttClient MqttClient_1 = null;
        private static MqttClient MqttClient_2 = null;
        private static string MQTTTopic_1;
        private static string MQTTTopic_2;
        private static MongoClient client = null;
        private static IMongoDatabase database = null;
        static HubConnection connection = null;
        private static IHubProxy appHub = null;
        private static string signalr_server = null;
        private int timesoff = 25330;
        SmartlightAllWidgetRealtime_REC WR = new SmartlightAllWidgetRealtime_REC();
        #endregion

        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                signalr_server = ConfigurationManager.AppSettings["signalr_server"].ToString();
                string mongoDBsvr = ConfigurationManager.AppSettings["mongodb_server"].ToString();
                string MQTTBroker_1 = ConfigurationManager.AppSettings["MQTTBroker_1"].ToString();
                string MQTTBroker_2 = ConfigurationManager.AppSettings["MQTTBroker_2"].ToString();
                MQTTTopic_1 = ConfigurationManager.AppSettings["MQTTTopic_1"].ToString();
                MQTTTopic_2 = ConfigurationManager.AppSettings["MQTTTopic_2"].ToString();
                string MQTTBrokerMultiple = ConfigurationManager.AppSettings["MQTTBrokerMultiple"].ToString();
                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        signalr_server = rootobj["signalr_server"].ToString();
                        mongoDBsvr = rootobj["mongodb_server"].ToString();
                        MQTTBroker_1 = rootobj["MQTTBroker_1"].ToString();
                        MQTTBroker_2 = rootobj["MQTTBroker_2"].ToString();
                        MQTTTopic_1 = rootobj["MQTTTopic_1"].ToString();
                        MQTTTopic_2 = rootobj["MQTTTopic_2"].ToString();
                        MQTTBrokerMultiple = rootobj["MQTTBrokerMultiple"].ToString();
                        timesoff = Convert.ToInt32(rootobj["timesoff"].ToString());
                    }
                }

                initRTHub();

                // mongoDB connection 
                client = new MongoClient(mongoDBsvr);
                database = client.GetDatabase("IoT");

                if (MQTTBrokerMultiple == "true")
                {
                    // mqtt connection 
                    MqttClient_1 = new MqttClient(MQTTBroker_1);
                    MqttClient_1.MqttMsgPublishReceived += client_MqttMsgPublishReceived_1;
                    MqttClient_1.Subscribe(new string[] { MQTTTopic_1 + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                    string clientId1 = Guid.NewGuid().ToString();
                    MqttClient_1.Connect(clientId1);

                    void client_MqttMsgPublishReceived_1(object sender, MqttMsgPublishEventArgs e)
                    {
                        var message = System.Text.Encoding.Default.GetString(e.Message);
                        if (IsValidJson(message))
                        {
                            object result = JsonConvert.DeserializeObject(message);
                            JObject voobj = JObject.Parse(result.ToString());

                            string[] topic = e.Topic.Split('/');
                            string Category = topic[0];
                            string hostDevice = topic[1];
                            if (topic.Length > 1)
                            {
                                string cmd = topic[3];
                                switch (cmd)
                                {
                                    case "data":
                                        SensorHandler(voobj, topic);
                                        break;
                                    default:

                                        break;
                                }
                            }
                        }
                    }
                }

                // mqtt connection apps
                MqttClient_2 = new MqttClient(MQTTBroker_2);
                MqttClient_2.MqttMsgPublishReceived += client_MqttMsgPublishReceived_2;
                MqttClient_2.Subscribe(new string[] { MQTTTopic_2 + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                string clientId_2 = Guid.NewGuid().ToString();
                MqttClient_2.Connect(clientId_2);

                void client_MqttMsgPublishReceived_2(object sender, MqttMsgPublishEventArgs e)
                {
                    string[] topic = e.Topic.Split('/');
                    string Category = topic[0];
                    string hostDevice = topic[1];
                    var message = System.Text.Encoding.Default.GetString(e.Message);
                    if (IsValidJson(message))
                    {
                        object result = JsonConvert.DeserializeObject(message);
                        JObject voobj = JObject.Parse(result.ToString());

                        if (topic.Length > 1)
                        {
                            string cmd = topic[3];
                            switch (cmd)
                            {
                                case "data":
                                    SensorHandler(voobj, topic);
                                    break;
                                case "SetConfig":
                                    setConfig(message);
                                    break;
                                case "GetConfig":
                                    getConfig(message);
                                    break;
                                default:

                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (topic.Length > 1 && MQTTBrokerMultiple == "true")
                        {
                            string cmd = topic[3];
                            switch (cmd)
                            {
                                case "relay":
                                    MqttClient_1.Publish(e.Topic, e.Message, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                    break;
                                default:

                                    break;
                            }
                        }
                    }
                }

                Thread t = new Thread(new ThreadStart(ProcessData));
                Thread p = new Thread(new ThreadStart(Publisher));
                t.Start();
                p.Start();
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public void ProcessData()
        {
            try
            {
                while (true)
                {
                    var collection = database.GetCollection<SmartWidget_REC>("SmartlightDataLogs");
                    var CollectDevice = database.GetCollection<SmartDeviceView_REC>("SmartlightDevices");
                    var widgetRealtimeAllColl = database.GetCollection<SmartlightAllWidgetRealtime_REC>("SmarlightAllWidgetRealtime");
                    var widgetRealtimeSpecificColl = database.GetCollection<SmartlightSpecificWidgetRealtime_REC>("SmartlightSpecificWidgetRealtime");

                    #region counting device online/offline
                    int DeviceTotal = 0;
                    int DeviceOn = 0;
                    int DeviceOff = 0;
                    foreach (SmartDeviceView_REC voData in CollectDevice.Find(_ => true).ToList())
                    {
                        double TotalSeconds = (DateTime.Now - voData.RecordTimestamp).TotalSeconds;
                        if (TotalSeconds > timesoff)
                        {
                            // update Device status
                            var builder = Builders<SmartDeviceView_REC>.Filter;
                            var flt = builder.Eq<string>("IPAddress", voData.IPAddress);
                            var update = Builders<SmartDeviceView_REC>.Update.Set("RecordTimestamp", DateTime.Now).Set("Status", 0);
                            CollectDevice.UpdateOne(flt, update);
                        }

                        DeviceTotal += 1;
                        if (voData.Status == 1) { DeviceOn += 1; } else { DeviceOff += 1; }
                    }
                    #endregion

                    #region get total usage all device daily
                    var filter = new BsonDocument
                    {
                        {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                        {"$lte", DateTime.Now}
                    };
                    string match = "{$match: {RecordTimestamp : " + filter + " }}";
                    string grp = "{$group:{_id:{DeviceID:\"$DeviceID\"},ApparentPower:{$sum:{$toDecimal:\"$ActivePower\"}}}}";
                    var pipeline = collection.Aggregate().AppendStage<SmartWidget_REC>(match).AppendStage<SmartWidget_REC>(grp);
                    var res = pipeline.ToList();
                    decimal Total = 0;
                    if (res.Count != 0)
                    {
                        foreach (SmartWidget_REC voData in res)
                        {
                            Total += voData.ApparentPower;
                        }
                    }
                    double hours = (DateTime.Now - DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))).TotalHours;
                    decimal voTotal = Convert.ToDecimal(Total) / Convert.ToDecimal(hours) / 1000;

                    // begin usage all device save to collection
                    var bld = Builders<SmartlightAllWidgetRealtime_REC>.Filter;
                    var fltr = bld.Eq<int>("recordID", 1);
                    WR = widgetRealtimeAllColl.Find(fltr).SingleOrDefault();
                    SmartlightAllWidgetRealtime_REC usageTotal = new SmartlightAllWidgetRealtime_REC();
                    usageTotal.recordID = 1;
                    usageTotal.totalUsage = voTotal.ToString("0.##");
                    usageTotal.deviceTotal = DeviceTotal.ToString();
                    usageTotal.deviceOn = DeviceOn.ToString();
                    usageTotal.deviceOff = DeviceOff.ToString();
                    if (WR == null)
                    {
                        widgetRealtimeAllColl.InsertOne(usageTotal);
                    }
                    else
                    {
                        var update = Builders<SmartlightAllWidgetRealtime_REC>.Update.Set("totalUsage", usageTotal.totalUsage)
                            .Set("deviceTotal", usageTotal.deviceTotal)
                            .Set("deviceOn", usageTotal.deviceOn)
                            .Set("deviceOff", usageTotal.deviceOff);
                        widgetRealtimeAllColl.UpdateOne(fltr, update);
                    }
                    // end widget all device

                    #endregion

                    var filterm = new BsonDocument
                    {
                        {"$gte", DateTime.Parse(DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd 00:00:00"))},
                        {"$lte", DateTime.Now}
                    };
                    string matchm = "{$match: {RecordTimestamp : " + filterm + " }}";
                    string grpm = "{$group:{_id:{DeviceID:\"$DeviceID\"},ApparentPower:{$sum:{$toDecimal:\"$ActivePower\"}}}}";

                    var pipelinem = collection.Aggregate().AppendStage<SmartWidget_REC>(matchm).AppendStage<SmartWidget_REC>(grpm);
                    var resm = pipelinem.ToList();
                    if (resm.Count > 0)
                    {
                        foreach (var DeviceData in resm)
                        {
                            SmartlightSpecificWidgetRealtime_REC specificDev = new SmartlightSpecificWidgetRealtime_REC();
                            specificDev.TotalMonth = DeviceData.ApparentPower / 1000;
                            specificDev.TotalToday = voTotal;
                            specificDev.MonthlyAvg = specificDev.TotalMonth / 720;
                            specificDev.DailyAvg = specificDev.TotalMonth / 24;
                            specificDev.IPAddress = DeviceData._id.DeviceID;

                            var bld1 = Builders<SmartlightSpecificWidgetRealtime_REC>.Filter;
                            var fltr1 = bld1.Eq<string>("IPAddress", DeviceData._id.DeviceID);
                            var spec = widgetRealtimeSpecificColl.Find(fltr1).SingleOrDefault();
                            if (spec == null)
                            {
                                widgetRealtimeSpecificColl.InsertOne(specificDev);
                            }
                            else
                            {
                                var update = Builders<SmartlightSpecificWidgetRealtime_REC>.Update.Set("TotalMonth", specificDev.TotalMonth)
                                .Set("TotalToday", specificDev.TotalToday)
                                .Set("MonthlyAvg", specificDev.MonthlyAvg)
                                .Set("DailyAvg", specificDev.DailyAvg);
                                widgetRealtimeSpecificColl.UpdateOne(fltr1, update);
                            }
                        }
                    }
                    Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public void Publisher()
        {
            while (true)
            {
                #region publish main widget smartplug
                Dictionary<string, string> ResultData = new Dictionary<string, string>();
                if (WR != null)
                {
                    ResultData.Add("Total", WR.totalUsage);
                    ResultData.Add("TotalDevice", WR.deviceTotal);
                    ResultData.Add("DeviceOn", WR.deviceOn);
                    ResultData.Add("DeviceOff", WR.deviceOff);
                }
                else
                {
                    ResultData.Add("Total", "0");
                    ResultData.Add("TotalDevice", "0");
                    ResultData.Add("DeviceOn", "0");
                    ResultData.Add("DeviceOff", "0");
                }
                string sv = Convert.ToString(JsonConvert.SerializeObject(ResultData));
                heandlerSignalR("WidgetRealtime", "Smartlight", sv);
                //MqttClient_2.Publish("WidgetRealtime/Smartlight", Encoding.UTF8.GetBytes(sv), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                #endregion
                Thread.Sleep(6000);
            }
        }
        public static void SensorHandler(JObject obj, string[] topic)
        {
            try
            {
                string DeviceID = topic[2];
                string Voltage = (obj["voltage"] == null) ? "0" : obj["voltage"].ToString().ToString();
                string Current = (obj["current"] == null) ? "0" : obj["current"].ToString();
                string Power = (obj["power"] == null) ? "0" : obj["power"].ToString();
                string Apparent = (obj["apparent"] == null) ? "0" : obj["apparent"].ToString();
                string Factor = (obj["factor"] == null) ? "0" : obj["factor"].ToString();
                string Energy = (obj["energy"] == null) ? "0" : obj["energy"].ToString();
                string Relay = (obj["relay/0"] == null) ? "" : obj["relay/0"].ToString();
                string Host = (obj["host"] == null) ? "" : obj["host"].ToString();
                string IP = (obj["ip"] == null) ? DeviceID : obj["ip"].ToString();
                string MAC = (obj["mac"] == null) ? "" : obj["mac"].ToString();
                // begin switch relay
                SwitchRelay voSwitch = new SwitchRelay();
                voSwitch.Status = Relay;
                voSwitch.IPAddress = topic[2].ToString();
                if (Relay != "")
                {
                    string spa = Convert.ToString(JsonConvert.SerializeObject(voSwitch));
                    heandlerSignalR("WidgetRealtime", "SmartlightSwitch", spa);

                    var SmartDeviceColl = database.GetCollection<SmartDevices_REC>("SmartDevices");

                    var bld1 = Builders<SmartDevices_REC>.Filter;
                    var flt1 = bld1.Eq<string>("IPAddress", IP);
                    if (voSwitch.Status == "1" || voSwitch.Status == "ON")
                    {
                        var up1 = Builders<SmartDevices_REC>.Update.Set("Relay", 1);
                        SmartDeviceColl.UpdateOne(flt1, up1);
                    }
                    else
                    {
                        var up1 = Builders<SmartDevices_REC>.Update.Set("Relay", 0);
                        SmartDeviceColl.UpdateOne(flt1, up1);
                    }
                    //MqttClient_2.Publish("WidgetRealtime/SmartlightSwitch", Encoding.UTF8.GetBytes(spa), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
                // end switch relay
                var DataLogs = database.GetCollection<SmartLog_REC>("SmartlightDataLogs");
                var collection = database.GetCollection<SmartWidget_REC>("SmartlightDataLogs");
                var InserDevice = database.GetCollection<SmartDevice_REC>("SmartlightDevices");
                var CollectDevice = database.GetCollection<SmartDeviceView_REC>("SmartlightDevices");
                var widgetRealtimeSpecificColl = database.GetCollection<SmartlightSpecificWidgetRealtime_REC>("SmartlightSpecificWidgetRealtime");

                // begin insert log
                SmartLog_REC oData = new SmartLog_REC();
                oData.RecordTimestamp = DateTime.Now;
                oData.DeviceID = DeviceID;
                oData.IPAddress = IP;
                oData.Voltage = Convert.ToInt32(Voltage);
                oData.ActivePower = Convert.ToInt32(Power);
                oData.ApparentPower = Convert.ToInt32(Apparent);
                oData.PowerFactor = Convert.ToInt32(Factor);
                oData.Energy = Convert.ToDecimal(Energy);
                oData.Current = Convert.ToDecimal(Current);
                DataLogs.InsertOne(oData);
                // end insert log

                
                SmartChart_REC poData = new SmartChart_REC();
                poData.RecordTimestamp = DateTime.Now;
                poData.DeviceID = DeviceID;
                poData.IPAddress = IP;
                poData.Voltage = Convert.ToInt32(Voltage);
                poData.ActivePower = Convert.ToInt32(Power);
                poData.ApparentPower = Convert.ToInt32(Apparent);
                poData.PowerFactor = Convert.ToInt32(Factor);
                poData.Energy = Convert.ToDecimal(Energy);
                poData.Current = Convert.ToDecimal(Current);

                var bldt = Builders<SmartlightSpecificWidgetRealtime_REC>.Filter;
                var fltt = bldt.Eq<string>("IPAddress", poData.IPAddress);
                var WidgetSpecific = widgetRealtimeSpecificColl.Find(fltt).SingleOrDefault();
                if (WidgetSpecific != null)
                {
                    poData.TotalMonth = WidgetSpecific.TotalMonth;
                    poData.TotalToday = WidgetSpecific.TotalToday;
                    poData.MonthlyAvg = WidgetSpecific.MonthlyAvg;
                    poData.DailyAvg = WidgetSpecific.DailyAvg;
                }
                else
                {
                    poData.TotalMonth = Convert.ToDecimal(0.00);
                    poData.TotalToday = Convert.ToDecimal(0.00);
                    poData.MonthlyAvg = Convert.ToDecimal(0.00);
                    poData.DailyAvg = Convert.ToDecimal(0.00);
                }
                string sp = Convert.ToString(JsonConvert.SerializeObject(poData));
                heandlerSignalR("WidgetRealtime", "SmartlightDevice", sp);
                //MqttClient_2.Publish("WidgetRealtime/SmartlightDevice", Encoding.UTF8.GetBytes(sp), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                // hearbeat device
                var builder = Builders<SmartDeviceView_REC>.Filter;
                var flt = builder.Eq<string>("IPAddress", IP);
                var resData = CollectDevice.Find(flt).SingleOrDefault();
                if (resData == null)
                {
                    SmartDevice_REC voDevice = new SmartDevice_REC();
                    voDevice.RecordTimestamp = DateTime.Now;
                    voDevice.DeviceID = DeviceID;
                    voDevice.IPAddress = IP;
                    voDevice.Status = 1;
                    voDevice.Relay = 1;
                    InserDevice.InsertOne(voDevice);
                }
                else
                {
                    var update = Builders<SmartDeviceView_REC>.Update.Set("RecordTimestamp", DateTime.Now).Set("Status", 1);
                    CollectDevice.UpdateOne(flt, update);
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        private static void setConfig(string message)
        {
            try
            {
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());
                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";

                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("mongodb_server", voobj["mongodb_server"].ToString());
                voResData.Add("signalr_server", voobj["signalr_server"].ToString());
                voResData.Add("MQTTBroker_1", voobj["MQTTBroker_1"].ToString());
                voResData.Add("MQTTBroker_2", voobj["MQTTBroker_2"].ToString());
                voResData.Add("MQTTTopic_1", voobj["MQTTTopic_1"].ToString());
                voResData.Add("MQTTTopic_2", voobj["MQTTTopic_2"].ToString());
                voResData.Add("MQTTBrokerMultiple", voobj["MQTTBrokerMultiple"].ToString());

                // begin write to json file
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                File.WriteAllText(basepatch, strValue);
                // end wriet to json file

                // publish mqtt
                MqttClient_2.Publish(MQTTTopic_2 + "All/setConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                // Restart Apps
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        private static void getConfig(string message)
        {
            try
            {
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());

                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        string mongodb_server = voobj["mongodb_server"].ToString();
                        string signalr_server = voobj["signalr_server"].ToString();
                        string MQTTBroker_1 = rootobj["MQTTBroker_1"].ToString();
                        string MQTTBroker_2 = rootobj["MQTTBroker_2"].ToString();
                        string MQTTTopic_1 = rootobj["MQTTTopic_1"].ToString();
                        string MQTTTopic_2 = rootobj["MQTTTopic_2"].ToString();
                        string MQTTBrokerMultiple = rootobj["MQTTBrokerMultiple"].ToString();
                        // publish mqtt
                        Dictionary<string, string> voResData = new Dictionary<string, string>();
                        voResData.Add("mongodb_server", mongodb_server);
                        voResData.Add("signalr_server", signalr_server);
                        voResData.Add("MQTTBroker_1", MQTTBroker_1);
                        voResData.Add("MQTTBroker_2", MQTTBroker_2);
                        voResData.Add("MQTTTopic_1", MQTTTopic_1);
                        voResData.Add("MQTTTopic_2", MQTTTopic_2);
                        voResData.Add("MQTTBrokerMultiple", MQTTBrokerMultiple);
                        string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                        MqttClient_2.Publish(MQTTTopic_2 + "All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    }
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        protected override void OnStop()
        {
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "smartlight");
            voResData.Add("msg", "HASIOT - smartlight service is stopped");
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient_2.Publish(MQTTTopic_2 + "All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }
        #region snipet
        static void RestartApp(int pid, string applicationName)
        {
            // Wait for the process to terminate
            Process process = null;
            try
            {
                process = Process.GetProcessById(pid);
                process.WaitForExit(1000);
            }
            catch (ArgumentException ex)
            {
                errorHandler(ex.Message);
            }
            Process.Start(applicationName, "");
        }
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        static void errorHandler(string Message)
        {
            // sending error messages
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "All");
            voResData.Add("msg", Message);
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient_2.Publish(MQTTTopic_2 + "All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            // Restart service
            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;
            string applicationName = currentProcess.ProcessName;
            RestartApp(pid, applicationName);
            System.Environment.Exit(1);
        }
        private static void initRTHub()
        {
            try
            {
                //Set connection
                connection = new HubConnection(signalr_server);
                //Make proxy to hub based on hub name on server
                appHub = connection.CreateHubProxy("apphub");
                //Start connection

                connection.Start().ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        task.Exception.GetBaseException();
                    }
                    else
                    {
                        Console.WriteLine("Connected");
                    }
                }).Wait();
            }
            catch (Exception ex)
            {

            }
        }
        private static void heandlerSignalR(string mod, string tp, string sp)
        {
            try
            {
                appHub.Invoke<string>("Send", mod, tp, sp).ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}",
                                          task.Exception.GetBaseException());
                        if (connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                        {
                            initRTHub();
                            heandlerSignalR(mod, tp, sp);
                        }
                    }
                    else
                    {
                        Console.WriteLine(task.Result);
                    }
                });
            }
            catch (Exception ex)
            {
                if (connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                {
                    initRTHub();
                    heandlerSignalR(mod, tp, sp);
                }
            }
        }
        #endregion
    }
}
