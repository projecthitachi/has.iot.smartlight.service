﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace has.iot.smartlight.service
{
    public class SwitchRelay
    {
        public string Status { get; set; }
        public string IPAddress { get; set; }
    }
    public class SmartLog_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public int ActivePower { get; set; }
        public int ApparentPower { get; set; }
    }

    public class SmartChart_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public int ActivePower { get; set; }
        public int ApparentPower { get; set; }
        public string IPAddress { get; set; }
        public string fullcharger { get; set; }
        public string GadGet { get; set; }
        public decimal TotalToday { get; set; }
        public decimal TotalMonth { get; set; }
        public decimal DailyAvg { get; set; }
        public decimal MonthlyAvg { get; set; }
    }

    public class SmartDevice_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        //public int TotalUsage { get; set; }
    }
    public class SmartDeviceView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        public int TotalUsage { get; set; }
    }
    public class SmartWidget_REC
    {
        public smart_id _id { get; set; }
        public decimal ApparentPower { get; set; }
    }
    public class smart_id
    {
        public string DeviceID { get; set; }
    }

    public class SmartlightAllWidgetRealtime_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int recordID { get; set; }
        public string totalUsage { get; set; }
        public string deviceTotal { get; set; }
        public string deviceOn { get; set; }
        public string deviceOff { get; set; }
    }

    public class SmartlightSpecificWidgetRealtime_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string IPAddress { get; set; }
        public string fullcharger { get; set; }
        public string GadGet { get; set; }
        public decimal TotalToday { get; set; }
        public decimal TotalMonth { get; set; }
        public decimal DailyAvg { get; set; }
        public decimal MonthlyAvg { get; set; }
    }

    public class SmartDevices_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
    }
}
